# Flexbox Tutorial

Flexbox is a new box model for CSS. We modify the properties of flexbox in the CSS file associated with the webpage .

## Using flexbox in your project

  - A container element should be there for holding the child elements.
  - In CSS the 'display' property of the container element is set to flex.
 
 ```css
 .container{
     /* General format */
	display: flex;
     /* For more browser support */
	display: -webkit-flex;
	display: -moz-flex;
	display: -ms-flex;
	display: -o-flex;
 }
 ```
 
## Concepts of Axes

There are two types of axes in flexbox :

- Main axes : It is like the row or the horizontal axis.
- Cross axes : It is vertical axis as it is perpendicular to main axis.

## Properties

####  *flex-direction*

###### Values :

- row : Arranges child elements along the main-axis (horizontal axis).
- row-reverse : Arranges elements across the main-axis (horizontal axis) in reverse order.
- column : Arranges the child elements along the cross-axis (vertical axis).
- column-reverse : Arranges elements across the cross-axis (vertical axis) in reverse order.

You can use the **float** property to place the container element to a part of the screen *left* or *right*. You can specify a **height** and **width** to the container to actually see it being moved while giving value to **float** property

```css
.container{
    height: 200px;
    width: 500px;
    display: flex;
    flex-direction: row;    
    float: left;
}
```

#### *justify-content*

Used to justify content along the main axis (horizontal axis).

###### Values :

- flex-start : For `flex-direction: row; ` it will align the children to the **left** and for `flex-direction: row-reverse;` it will align the content to the **right** side of the container.
- flex-end : For `flex-direction: row; ` it will align the children to the **right** and for `flex-direction: row-reverse;` it will align the content to the **left** side of the container.
- center : It will align the child elements to the horizontal center (across the main axis) of the container element.
- space-around : Each element has same spacing to its **left and right sides**.
- space-between : Each element has same spacing **between them and to both ends**.

```css
.container{
    height: 200px;
    width: 100%;
    display: flex;
    flex-direction: row;
    justify-content: flex-end;
}
```



> *You can try out flexbox styling at [Flexbox Froggy](https://flexboxfroggy.com/) by playing the game of bringing a frog onto lily pads by changing flexbox properties.*



#### *align-items*

This property can be used to set the line to which the base/top of **all** the elements within that container aligns to.

###### Values :

- center : Aligns items to the vertical center of the container. The neither the base not the top of different sized children will be in the same line. Their centers will be in the same line instead.
- baseline : The bases of all the children including different sized children will be in the same line. Starts from the top of the container.
- stretch : Stretches the child elements in order to match the height of the container in `flex-direction: row;`
- flex-start : Aligns the top of child elements in a straight line. Starts from the top of the container.
- flex-end : Aligns the base of child elements in a straight line. Starts from the bottom of the container.

#### *flex-wrap*

To wrap overflowing elements within the container we use the *flex-wrap* property.

###### Values :

- nowrap : Default. Contents will overflow if there is no sufficient space for them.
- wrap : Flex box will make items float into the next line preventing overflow. Use *align-items* to align the wrapped items properly.
- wrap-reverse : Rows are filled in reverse order. Last row is filled first and when it overflows, content is wrapped into the row above.

> The properties flex-wrap and flex-direction are often used together so there is another property flex-flow that combines these two.
>
> **Syntax :**  `flex-flow: [flex-direction-value] [flex-wrap-value]`



#### *align-content*

Used to justify content along the cross axis (vertical axis).

###### Values :

- flex-start : For `flex-flow: row wrap; ` it will align the children to the **top - left** and for `flex-flow: row-reverse wrap;` it will align the content to the **top - right** side of the container. Similarly for `flex-flow: row wrap-reverse; ` it will align the children to the **bottom - left** and for `flex-flow: row-reverse wrap-reverse;` it will align the content to the **bottom - right** side of the container.
- flex-end : For `flex-flow: row wrap; ` it will align the children to the **bottom - left** and for `flex-flow: row-reverse wrap;` it will align the content to the **bottom - right** side of the container. Similarly for `flex-flow: row wrap-reverse; ` it will align the children to the **top - left** and for `flex-flow: row-reverse wrap-reverse;` it will align the content to the **top - right** side of the container.
- center : It will align the child elements to the center of the container element.
- space-around : Each element has same spacing to its **top and bottom**.
- space-between : Each element has same spacing **between them and to both top and bottom ends**.

```css
 .container{
     height: 700px;
     width: 500px;
     display: flex;
     flex-flow: row wrap;
     align-content: flex-end;
}
```

#### *order*

Used to change the order in which the elements appear. By default all the elements have the order zero (`order: 0;` ). So we can select an element and change its order by specifying the order property. Elements are displayed in the ascending order of their *order* property's value.

###### Values :

POSITIVE (eg: 2) : Will increment the order of the element to 2.

NEGATIVE (eg: -1) : Will decrement the order of the element to -1.

```css
.container div:nth-child(2){
    order: 2;
}
.container div:nth-child(3){
    order: -1;
}
```

#### *align-self*

Used to align individual components across the cross-axis (vertical axis).

###### Values:

- flex-start : Aligns the selected element to the top of the container.
- flex-end: Aligns the selected element to the bottom of the container.
- center : Aligns the selected element to the center of the container.
- stretch : Stretches the element to fill up the container across the cross-axis.
- baseline : Aligns the selected element to its baseline.

#### *flex-grow*

This property is used to adjust the child element size when the parent element has more space than required.

###### Values :

- 0 (ZERO) : This is the default value. The child elements will not grow in order to fill up the excess space available in the container.
- POSITIVE :
  - We can give **any** positive value for the **whole set **of children.
  - We can give **any** positive value for **selected** children.

Consider that we have five elements and we are giving `flex-grow: 1;` to all the for elements and `flex-grow: 2;` to the third element. So at first the entire size of the container is divided into five **equal** parts and each element is getting the same size. When the third element is selected and assigned `flex-grow: 2;` all the other four elements are having `flex-grow: 1;` . So the total sum will be 
$$
4*1 + 2*1 = 6
$$
So the entire size (here it is width) is divided into 6 equal parts. The third element gets 2 parts. If the total size id 600px, the third element gets 200px and all others get 100px each.

```css
.container{
    width: 600px;
    display: flex;
    flex-flow: row;
}
.container div{
    float: right;
    width: 50px;
    margin: 0;
    flex-grow: 1;
}
.container div:nth-child(3){
    flex-grow: 2;
}
```

